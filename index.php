<?php require_once('template/website/header.php') ?>

<main class="main__content_wrapper">
   <!-- Start slider section -->
   <section class="hero__slider--section">
      <div class="hero__slider--inner hero__slider--activation swiper">
         <div class="hero__slider--wrapper swiper-wrapper">
            <div class="swiper-slide ">
               <div class="hero__slider--items home1__slider--bg">
                  <div class="container-fluid">
                     <div class="hero__slider--items__inner">
                        <div class="row row-cols-1">
                           <div class="col">
                              <div class="slider__content">
                                 <p class="slider__content--desc desc1 mb-15 text-white"><img class="slider__text--shape__icon" src="assets/website/img/icon/text-shape-icon.png" alt="text-shape-icon"> New Collection</p>
                                 <h2 class="slider__content--maintitle h1 text-white">The Great Fashion <br>
                                    Collection 2022</h2>
                                 <p class="slider__content--desc desc2 d-sm-2-none mb-40 text-white">Up To 40% Off Final Sale Items. <br>
                                    Caught in the Moment!</p>
                                 <a class="slider__btn primary__btn" href="order.php">Pesan Sekarang
                                    <svg class="primary__btn--arrow__icon" xmlns="http://www.w3.org/2000/svg" width="20.2" height="12.2" viewBox="0 0 6.2 6.2">
                                       <path d="M7.1,4l-.546.546L8.716,6.713H4v.775H8.716L6.554,9.654,7.1,10.2,9.233,8.067,10.2,7.1Z" transform="translate(-4 -4)" fill="currentColor"></path>
                                    </svg>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="swiper-slide ">
               <div class="hero__slider--items home1__slider--bg two">
                  <div class="container-fluid">
                     <div class="row">
                        <div class="col">
                           <div class="hero__slider--items__inner">
                              <div class="slider__content">
                                 <p class="slider__content--desc desc1 mb-15 text-white"><img class="slider__text--shape__icon" src="assets/website/img/icon/text-shape-icon.png" alt="text-shape-icon"> New Collection</p>
                                 <h2 class="slider__content--maintitle h1 text-white">The Great Fashion <br>
                                    Collection 2022</h2>
                                 <p class="slider__content--desc desc2 d-sm-2-none mb-40 text-white">Up To 40% Off Final Sale Items. <br>
                                    Caught in the Moment!</p>
                                 <a class="primary__btn slider__btn" href="order.php">Pesan Sekarang
                                    <svg class="slider__btn--arrow__icon" xmlns="http://www.w3.org/2000/svg" width="20.2" height="12.2" viewBox="0 0 6.2 6.2">
                                       <path d="M7.1,4l-.546.546L8.716,6.713H4v.775H8.716L6.554,9.654,7.1,10.2,9.233,8.067,10.2,7.1Z" transform="translate(-4 -4)" fill="currentColor"></path>
                                    </svg>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="swiper-slide ">
               <div class="hero__slider--items home1__slider--bg three">
                  <div class="container-fluid">
                     <div class="row">
                        <div class="col-lg-6 offset-lg-6">
                           <div class="hero__slider--items__inner">
                              <div class="slider__content text-center">
                                 <p class="slider__content--desc desc1 mb-15 text-white"><img class="slider__text--shape__icon" src="assets/website/img/icon/text-shape-icon.png" alt="text-shape-icon"> New Collection</p>
                                 <h2 class="slider__content--maintitle h1 text-white">The Great Fashion <br>
                                    Collection 2022</h2>
                                 <p class="slider__content--desc desc2 d-sm-2-none mb-40 text-white">Up To 40% Off Final Sale Items. <br>
                                    Caught in the Moment!</p>
                                 <a class="primary__btn slider__btn" href="order.php">Pesan Sekarang
                                    <svg class="slider__btn--arrow__icon" xmlns="http://www.w3.org/2000/svg" width="20.2" height="12.2" viewBox="0 0 6.2 6.2">
                                       <path d="M7.1,4l-.546.546L8.716,6.713H4v.775H8.716L6.554,9.654,7.1,10.2,9.233,8.067,10.2,7.1Z" transform="translate(-4 -4)" fill="currentColor"></path>
                                    </svg>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
         <div class="swiper__nav--btn swiper-button-next"></div>
         <div class="swiper__nav--btn swiper-button-prev"></div>
      </div>
   </section>
   <br>
   <!-- End slider section -->

   <!-- Start product section -->
   <section class="product__section section--padding pt-0">
      <div class="container-fluid">
         <div class="section__heading text-center mb-35">
            <h2 class="section__heading--maintitle">New Products</h2>
         </div>
         <div class="tab_content">
            <div id="featured" class="tab_pane active show">
               <div class="product__section--inner">
                  <div class="row row-cols-xl-5 row-cols-lg-4 row-cols-md-3 row-cols-2 mb--n30">
                     <?php foreach ($productModel->findAll() as $pakaian) : ?>
                        <div class="col mb-30">
                           <div class="product__items ">
                              <div class="product__items--thumbnail">
                                 <a class="product__items--link" href="detail.php?id=<?php echo $pakaian->id ?>">
                                    <img class="product__items--img product__primary--img" src="admin/uploads/<?php echo $pakaian->image; ?>" alt="product-img">
                                 </a>
                                 <div class="product__badge">
                                    <span class="product__badge--items sale">Sale</span>
                                 </div>
                              </div>
                              <div class="product__items--content">
                                 <span class="product__items--content__subtitle"><?php echo $pakaian->tipe ?></span>
                                 <h3 class="product__items--content__title h4"><a href="detail.php?id=<?php echo $pakaian->id ?>"><?php echo $pakaian->nama ?></a></h3>
                                 <div class="product__items--price">
                                    <span class="current__price"><?php echo 'Rp. ' . number_format($pakaian->harga, 0, '', '.') ?></span>
                                 </div>
                                 <ul class="rating product__rating d-flex">
                                    <li class="rating__list">
                                       <span class="rating__list--icon">
                                          <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                             <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                          </svg>
                                       </span>
                                    </li>
                                    <li class="rating__list">
                                       <span class="rating__list--icon">
                                          <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                             <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                          </svg>
                                       </span>
                                    </li>
                                    <li class="rating__list">
                                       <span class="rating__list--icon">
                                          <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                             <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                          </svg>
                                       </span>
                                    </li>
                                    <li class="rating__list">
                                       <span class="rating__list--icon">
                                          <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                             <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                          </svg>
                                       </span>
                                    </li>
                                    <li class="rating__list">
                                       <span class="rating__list--icon">
                                          <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                             <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                          </svg>
                                       </span>
                                    </li>

                                 </ul>
                                 <ul class="product__items--action d-flex">
                                    <li class="product__items--action__list">
                                       <a class="product__items--action__btn add__to--cart" href="detail.php?id=<?php echo $pakaian->id ?>">
                                          <svg class="product__items--action__btn--svg" xmlns="http://www.w3.org/2000/svg" width="22.51" height="20.443" viewBox="0 0 14.706 13.534">
                                             <g transform="translate(0 0)">
                                                <g>
                                                   <path data-name="Path 16787" d="M4.738,472.271h7.814a.434.434,0,0,0,.414-.328l1.723-6.316a.466.466,0,0,0-.071-.4.424.424,0,0,0-.344-.179H3.745L3.437,463.6a.435.435,0,0,0-.421-.353H.431a.451.451,0,0,0,0,.9h2.24c.054.257,1.474,6.946,1.555,7.33a1.36,1.36,0,0,0-.779,1.242,1.326,1.326,0,0,0,1.293,1.354h7.812a.452.452,0,0,0,0-.9H4.74a.451.451,0,0,1,0-.9Zm8.966-6.317-1.477,5.414H5.085l-1.149-5.414Z" transform="translate(0 -463.248)" fill="currentColor"></path>
                                                   <path data-name="Path 16788" d="M5.5,478.8a1.294,1.294,0,1,0,1.293-1.353A1.325,1.325,0,0,0,5.5,478.8Zm1.293-.451a.452.452,0,1,1-.431.451A.442.442,0,0,1,6.793,478.352Z" transform="translate(-1.191 -466.622)" fill="currentColor"></path>
                                                   <path data-name="Path 16789" d="M13.273,478.8a1.294,1.294,0,1,0,1.293-1.353A1.325,1.325,0,0,0,13.273,478.8Zm1.293-.451a.452.452,0,1,1-.431.451A.442.442,0,0,1,14.566,478.352Z" transform="translate(-2.875 -466.622)" fill="currentColor"></path>
                                                </g>
                                             </g>
                                          </svg>
                                          <span class="add__to--cart__text"> + Pesan Sekarang</span>
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- End product section -->

   <!-- Start testimonial section -->
   <section class="testimonial__section section--padding pt-0">
      <div class="container-fluid">
         <div class="section__heading text-center mb-40">
            <h2 class="section__heading--maintitle">Our Clients Say</h2>
         </div>
         <div class="testimonial__section--inner testimonial__swiper--activation swiper">
            <div class="swiper-wrapper">
               <div class="swiper-slide">
                  <div class="testimonial__items text-center">
                     <div class="testimonial__items--thumbnail">
                        <img class="testimonial__items--thumbnail__img border-radius-50" src="assets/website/img/other/Icon1.png" alt="testimonial-img">
                     </div>
                     <div class="testimonial__items--content">
                        <h3 class="testimonial__items--title">Iqbaal Ramadhan</h3>
                        <span class="testimonial__items--subtitle">fashion</span>
                        <p class="testimonial__items--desc"> Produk Kerenlo sangat berkualitas, sangat diperhatikan mulai dari bahan sampai detail jahitan! </p>
                        <ul class="rating testimonial__rating d-flex justify-content-center">
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>

                        </ul>
                     </div>
                  </div>
               </div>
               <div class="swiper-slide">
                  <div class="testimonial__items text-center">
                     <div class="testimonial__items--thumbnail">
                        <img class="testimonial__items--thumbnail__img border-radius-50" src="assets/website/img/other/Icon2.png" alt="testimonial-img">
                     </div>
                     <div class="testimonial__items--content">
                        <h3 class="testimonial__items--title">Ariel</h3>
                        <span class="testimonial__items--subtitle">fashion</span>
                        <p class="testimonial__items--desc">Bahan yang sangat diperhatikan, sangat nyaman dipakai sehari-hari </p>
                        <ul class="rating testimonial__rating d-flex justify-content-center">
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>

                        </ul>
                     </div>
                  </div>
               </div>
               <div class="swiper-slide">
                  <div class="testimonial__items text-center">
                     <div class="testimonial__items--thumbnail">
                        <img class="testimonial__items--thumbnail__img border-radius-50" src="assets/website/img/other/Icon1.png" alt="testimonial-img">
                     </div>
                     <div class="testimonial__items--content">
                        <h3 class="testimonial__items--title">Rizky Febian</h3>
                        <span class="testimonial__items--subtitle">fashion</span>
                        <p class="testimonial__items--desc">Dipakai saat manggung sangat cocok, tinggal cara kita mix and match stylenya! </p>
                        <ul class="rating testimonial__rating d-flex justify-content-center">
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>

                        </ul>
                     </div>
                  </div>
               </div>
               <div class="swiper-slide">
                  <div class="testimonial__items text-center">
                     <div class="testimonial__items--thumbnail">
                        <img class="testimonial__items--thumbnail__img border-radius-50" src="assets/website/img/other/Icon2.png" alt="testimonial-img">
                     </div>
                     <div class="testimonial__items--content">
                        <h3 class="testimonial__items--title">Arief Muhammad</h3>
                        <span class="testimonial__items--subtitle">fashion</span>
                        <p class="testimonial__items--desc">Jahitan dari produknya sangat diperhatikan, Quality Control nya juga TOP! </p>
                        <ul class="rating testimonial__rating d-flex justify-content-center">
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>
                           <li class="rating__list">
                              <span class="rating__list--icon">
                                 <svg class="rating__list--icon__svg" xmlns="http://www.w3.org/2000/svg" width="14.105" height="14.732" viewBox="0 0 10.105 9.732">
                                    <path data-name="star - Copy" d="M9.837,3.5,6.73,3.039,5.338.179a.335.335,0,0,0-.571,0L3.375,3.039.268,3.5a.3.3,0,0,0-.178.514L2.347,6.242,1.813,9.4a.314.314,0,0,0,.464.316L5.052,8.232,7.827,9.712A.314.314,0,0,0,8.292,9.4L7.758,6.242l2.257-2.231A.3.3,0,0,0,9.837,3.5Z" transform="translate(0 -0.018)" fill="currentColor"></path>
                                 </svg>
                              </span>
                           </li>

                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="testimonial__pagination swiper-pagination"></div>
         </div>
      </div>
   </section>
   <!-- End testimonial section -->
</main>

<?php require_once('template/website/footer.php') ?>