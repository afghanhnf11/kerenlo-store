-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 14 Bulan Mei 2023 pada 10.49
-- Versi server: 8.0.30
-- Versi PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_toko_pakaian`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pakaian`
--

CREATE TABLE `pakaian` (
  `id` int NOT NULL,
  `nama` varchar(45) COLLATE utf8mb4_general_ci NOT NULL,
  `ukuran` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `warna` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `stok` int NOT NULL,
  `harga` int DEFAULT NULL,
  `tipe_pakaian_id` int DEFAULT NULL,
  `image` varchar(500) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `pakaian`
--

INSERT INTO `pakaian` (`id`, `nama`, `ukuran`, `warna`, `stok`, `harga`, `tipe_pakaian_id`, `image`) VALUES
(30, 'Nozan Shirt Long Sleeve Army', 'S, M, L, XL, XXL', 'Army', 100, 200000, 11, 'Produk-1.jpeg'),
(31, 'Nozan Shirt Long Sleeve Mocca', 'S, M, L, XL, XXL', 'Mocca', 100, 200000, 11, 'Produk-2.jpeg'),
(32, 'Vichy Shirt Long Sleeve Purple', 'S, M, L, XL, XXL', 'Purple', 100, 200000, 11, 'Produk-3.jpeg'),
(35, 'Tracktop Otenticiti Navy', 'S, M, L, XL, XXL', 'Navy', 100, 225000, 12, 'Produk-6.jpeg'),
(36, 'Stripy Shirt Long Sleeve Black', 'S, M, L, XL, XXL', 'Black', 100, 200000, 11, 'Produk-7.jpeg'),
(37, 'Ezypack Jacket Maroon', 'S, M, L, XL, XXL', 'Maroon', 100, 225000, 12, 'Produk8.jpeg'),
(38, 'Seersucker Overshirt Jacket Black', 'S, M, L, XL, XXL', 'Black', 100, 225000, 12, 'Produk9.jpeg'),
(39, 'Exmood EDP', '50 Ml', 'Eau De Parfume', 100, 150000, 13, 'Produk-10.jpeg'),
(40, 'Comfy Jacket Choco', 'S, M, L, XL, XXL', 'Choco', 100, 225000, 12, 'Produk-4.jpeg'),
(41, 'Comfy Jacket Oatmeal', 'S, M, L, XL, XXL', 'Oatmeal', 100, 225000, 12, 'Produk-5.jpeg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesanan`
--

CREATE TABLE `pesanan` (
  `id` int NOT NULL,
  `tanggal` date DEFAULT NULL,
  `pakaian_id` int DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `pesanan`
--

INSERT INTO `pesanan` (`id`, `tanggal`, `pakaian_id`, `quantity`, `nama`, `alamat`) VALUES
(6, '2023-05-13', NULL, 1, 'Afghan Hanif Adiyat', 'Jl. Pinang Jl. Margonda Raya No.9A, Pondok Cina, Kecamatan Beji, Kota Depok, Jawa Barat 16424'),
(7, '2023-05-13', NULL, 1, 'Afghan Hanif Adiyat', 'Jl. Pinang Jl. Margonda Raya No.9A, Pondok Cina, Kecamatan Beji, Kota Depok, Jawa Barat 16424'),
(11, '2023-05-14', 40, 1, 'Fadly Kalam Mustaqim', 'Jln. Sawo RT. 002 RW. 022 NO. 063 Kel. Mekarjaya Kec. Sukmajaya 16411 Kota Depok');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_pakaian`
--

CREATE TABLE `tipe_pakaian` (
  `id` int NOT NULL,
  `tipe` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `tipe_pakaian`
--

INSERT INTO `tipe_pakaian` (`id`, `tipe`) VALUES
(11, 'Shirt, Men'),
(12, 'Jacket, Men'),
(13, 'Parfume, Unisex');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `pakaian`
--
ALTER TABLE `pakaian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pakaian_1_idx` (`tipe_pakaian_id`);

--
-- Indeks untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pesanan1` (`pakaian_id`);

--
-- Indeks untuk tabel `tipe_pakaian`
--
ALTER TABLE `tipe_pakaian`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pakaian`
--
ALTER TABLE `pakaian`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tipe_pakaian`
--
ALTER TABLE `tipe_pakaian`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `pakaian`
--
ALTER TABLE `pakaian`
  ADD CONSTRAINT `fk_pakaian_tipe1` FOREIGN KEY (`tipe_pakaian_id`) REFERENCES `tipe_pakaian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  ADD CONSTRAINT `fk_pesanan1` FOREIGN KEY (`pakaian_id`) REFERENCES `pakaian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
