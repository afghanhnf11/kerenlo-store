<?php require_once('template/website/header.php'); ?>
<!-- fashion section start -->
<main class="main__content_wrapper">
    <div class="container mb-50">
        <form method="post" action="admin/process/order.php" style="margin-top: 20px;">
            <div class="checkout__content--step section__shipping--address">
                <div class="section__header mb-25">
                    <h3 class="section__header--title">Shipping address</h3>
                </div>
                <div class="section__shipping--address__content">
                    <div class="row">
                        <div class="col-12 mb-12">
                            <div class="checkout__input--list ">
                                <label>
                                    <input id="name" name="nama" type="text" class="checkout__input--field border-radius-5" placeholder="Nama Lengkap">
                                </label>
                            </div>
                        </div>
                        <div class="col-12 mb-12">
                            <div class="checkout__input--list">
                                <label>
                                    <input name="alamat" id="alamat" type="text" placeholder="Alamat Lengkap" class="checkout__input--field border-radius-5" required="required">
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-12">
                            <div class="checkout__input--list checkout__input--select select">
                                <label class="checkout__select--label" for="product">Pesanan</label>
                                <select class="checkout__input--select__field border-radius-5" id="product" name="pakaian_id">
                                    <option disabled selected>-- Pilih Produk --</option>
                                    <?php foreach ($productModel->findAll() as $pakaian) : ?>
                                        <option value="<?php echo $pakaian->id ?>"><?php echo $pakaian->nama ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-12">
                            <div class="checkout__input--list">
                                <label>
                                    <input id="quantity" name="quantity" type="number" class="checkout__input--field border-radius-5" placeholder="Jumlah Pesanan" required="required">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="checkout__content--step__footer d-flex align-items-center">
                <button class="continue__shipping--btn primary__btn border-radius-5" name="create_order" type="submit">Continue To Shipping</button>
            </div>
        </form>
    </div>
</main>

<?php require_once('template/website/footer.php') ?>