<?php

// BOOSTRAP CONFIGURASI DATABASE
require_once('app/config.php');
require_once('app/database.php');

// CLASS FILE PHP
require_once('class/Order.php');
require_once('class/Product.php');
require_once('class/TypeProduct.php');

// MODEL PHP
$orderModel = new Order($dbh);
$typeProductModel = new TypeProduct($dbh);
$productModel = new Product($dbh);
