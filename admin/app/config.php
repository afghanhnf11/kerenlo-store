<?php

// KONFIGURASI BACKEND ADMIN

// NAMA APLIKASI
define('APP_NAME', 'KERENLO Online Store');

// AKSES DATABASE
define('DB_NAME', 'db_toko_pakaian');
define('DB_USER', 'root');
define('DB_PASS', '');
define('LANDING_PATH', '../../index.php');

// FOLDER UPLOAD
$dir = __DIR__;
$dir = explode('/', $dir);
array_pop($dir);
$dir = implode('/', $dir);
define('UPLOADED_PATH', $dir);
